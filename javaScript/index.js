import { books } from "./array_books.js";
import { CreateLi } from "./CreateLi.js";

const keys = ["author", "name", "price"];

const bookList = books.map(book => {
    try {
        if (!book.author || !book.name || !book.price) {
            const diff = keys.filter(item => !Object.keys(book).includes(item))[0];
            throw new Error(`There is no '${diff}' value. Invalid book object: ${JSON.stringify(book)}`);
        }

        return new CreateLi(book.author, book.name, book.price).render();

    } catch (error) {
        console.error(error);
        return '';
    }
}).join('');


const root = document.getElementById('root');
const ul = document.createElement('ul');
root.append(ul);
ul.innerHTML = bookList;

