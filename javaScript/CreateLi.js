export class CreateLi {
    constructor(author, name, price) {
        this.author = author;
        this.name = name;
        this.price = price;
    }

    render() {
        return `
      <li>
        <p>Author: ${this.author}</p>
        <p>Name: "${this.name}"</p>
        <p>Price: ${this.price} грн.</p>
      </li>
    `;
    }
}